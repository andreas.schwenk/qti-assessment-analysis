# qti-assessment-analysis

Converts online assessment results from QTI-XML to HTML.

Usage:

`python3 qti2html.py UNZIPPED_INPUT_PATH`

