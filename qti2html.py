# ------------------------------------------------------------------------------
#                            QTI ASSESSMENT ANALYSIS
#
# Converts online assessment results from QTI-XML to HTML.
# THIS PROJECT IS WORK-IN-PROGRESS
# A good programming style is not possible due to lack of time :-)
#
# Author:   Andreas Schwenk
# License:  MIT
# ------------------------------------------------------------------------------

import os, glob, sys, pprint # TODO: zipfile
import xml.etree.ElementTree as ET

if len(sys.argv) != 2:
	print("usage: python3 UNZIPPED_INPUT_PATH")
	sys.exit(-1)

path = sys.argv[1]
if not os.path.exists(path):
	print("input path does not exist!")
	sys.exit(-1)

q_path = glob.glob(path + "/*__qti_*.xml")[0]
res_path = glob.glob(path + "/*__results_*.xml")[0]

o_path = path + ".html" # o := output


assessment_title = ""
questions = []
results = dict()

# ========== PARSE QUESTIONS ==========
tree = ET.parse(q_path)
root = tree.getroot()
assessment_title = root.findall("assessment")[0].attrib["title"]
items = root.findall("assessment")[0].findall("section")[0].findall("item")
for item in items:
	question = dict()
	questions.append(question)
	# ----- metadata -----
	question["title"] = item.attrib['title']
	question["id"] = item.attrib['ident'].split("_")[-1]
	meta = item.findall("itemmetadata")[0].findall("qtimetadata")[0].\
		findall("qtimetadatafield")
	for m in meta:
		label = m.findall("fieldlabel")[0].text
		entry = m.findall("fieldentry")[0].text
		if label == "QUESTIONTYPE":
			question["type"] = entry
	# ----- cloze question -----
	if question["type"] == "CLOZE QUESTION":
		gaps = dict()
		r = item.findall("resprocessing")[0].findall("respcondition")
		for ri in r:
			v = ri.findall("conditionvar")[0].findall("varequal")[0]
			gap_idx = v.attrib["respident"].split("_")[-1]
			gap_text = v.text
			if gap_idx not in gaps:
				gaps[gap_idx] = []
			gaps[gap_idx].append(gap_text)
		question["gaps"] = gaps
	# ----- single choice -----
	if question["type"] == "SINGLE CHOICE QUESTION":
		correct = []
		r = item.findall("resprocessing")[0].findall("respcondition")
		for ri in r:
			correct.append(int(ri.findall("setvar")[0].text) > 0)
		question["correct"] = correct
	# ----- question text -----
	flow = item.findall("presentation")[0].findall("flow")[0]
	question["html"] = ""
	for flow_item in flow:
		if flow_item.tag == "material":
			question["html"] += flow_item.findall("mattext")[0].text
		elif flow_item.tag == "response_str":
			gap_idx = flow_item.attrib["ident"].split("_")[-1]
			question["html"] += ' $$'+str(gap_idx) + ' ';
		elif flow_item.tag == "response_lid":
			question["html"] += '<ul>\n'
			for i, rl in enumerate(\
					flow_item.findall("render_choice")[0].\
					findall("response_label")):
				question["html"] += '<li>$$' + str(i) + ' '
				question["html"] += rl.findall("material")[0].findall("mattext")[0].text
				question["html"] += '<br/></li>\n'
			question["html"] += '</ul>\n'

def getQuestion(id):
	for q in questions:
		if q["id"] == id:
			return q
	return None

# ========== PARSE RESULTS ==========
tree = ET.parse(res_path)
root = tree.getroot()
results["points"] = dict()
results["num_passes"] = dict()
results["working_time"] = dict()
for r in root.findall("tst_pass_result")[0].findall("row"):
	active_fi = r.attrib["active_fi"]
	results["num_passes"][active_fi] = int(r.attrib["pass"])+1
	# only the last pass is stored:
	results["points"][active_fi] = r.attrib["points"]
	results["working_time"][active_fi] = r.attrib["workingtime"]
for r in root.findall("tst_solutions")[0].findall("row"):
	question_id = r.attrib["question_fi"]
	question = getQuestion(question_id)
	user_id = r.attrib["active_fi"]
	if question["type"] == "SINGLE CHOICE QUESTION":
		#print(r.attrib)
		#sys.exit(-1)
		if "user_selection" not in question:
			question["user_selection"] = []
		question["user_selection"].append(int(r.attrib["value1"]))
	elif question["type"] == "CLOZE QUESTION":
		gap_id = r.attrib["value1"]
		gap_user_text = r.attrib["value2"]
		if "user_gaps" not in question:
			question["user_gaps"] = dict()
		if gap_id not in question["user_gaps"]:
			question["user_gaps"][gap_id] = []
		question["user_gaps"][gap_id].append(gap_user_text)

# ========== OUTPUT ==========
for q in questions:
	pprint.pprint(q)
pprint.pprint(results)

s = '''
<!DOCTYPE html>
<html lang="de">
<html>
	<head>
		<meta charset="utf-8">
		<title>$title</title>
	</head>
	<body>
		<h1>$title</h1>

$questions

	</body>
</html>
'''

s = s.replace("$title", assessment_title + ' (Auswertung)')

q = ''
for question in questions:
	if question["type"] in ["CLOZE QUESTION", "SINGLE CHOICE QUESTION"]:
		q += "<hr/><h3>" + question["title"] + "</h3>\n"
		q += question["html"]
		if question["type"] == "SINGLE CHOICE QUESTION":
			solution = question["correct"]
			for i, si in enumerate(solution):
				num = 0
				for j in question["user_selection"]:
					if i == j:
						num += 1
				if si:
					text = '<span style="color: green;"><b>Richtig (' + str(num) + ')</b></span>';
				else:
					text = '<span style="color: red;"><b>Falsch (' + str(num) +')</b></span>';
				q = q.replace("$$" + str(i), text);
		elif question["type"] == "CLOZE QUESTION":
			for key, value in question["gaps"].items():
				solution = value[0]
				list_noncorrect = []
				num_correct = 0
				for user_value in question["user_gaps"][key]:
					correct = False
					for correct_value in question["gaps"][key]:
						if user_value == correct_value:
							num_correct += 1
							correct = True
							break
					if not correct and user_value not in list_noncorrect:
						list_noncorrect.append(user_value)
					str_noncorrect = ""
					if len(list_noncorrect) > 0:
						str_noncorrect = "(falsch: "
						for i, item in enumerate(list_noncorrect):
							if i > 0:
								str_noncorrect += ", "
							str_noncorrect += item
						str_noncorrect += ")"
				q = q.replace('$$' + key, '<span style="color: green;"><b>'
	                + solution + " (" + str(num_correct) + ")" "</b></span> "
	                + '<span style="color: red;">' + str(str_noncorrect) + '</span>')

s = s.replace("$questions", q)

fo = open(o_path, "w")
fo.write(s)
fo.close()
